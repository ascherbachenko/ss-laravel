# Laravel #

### Installation ###
* php composer.phar create-project --prefer-dist laravel/laravel blog
* php artisan make:auth
* php artisan migrate

### Controller ###
* php artisan make:controller BlogController

### Model ###
* php artisan make:model Blog -m
* php artisan migrate

### Routers ###
* php artisan route:list