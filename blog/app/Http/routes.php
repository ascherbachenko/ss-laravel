<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/blog', array('as' => 'blog', 'uses' => 'BlogController@index'));
Route::get('/blog/create', array('as' => 'blog.create', 'uses' => 'BlogController@create'));

Route::post('/add', array('as' => 'blog.store', 'uses' => 'BlogController@store'));
// Route::resource('blog', 'BlogController'); // defines all actions in a proper manner!!!
