@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Create</h1>

        {!! Form::open(array('route' => 'blog.store')) !!}
            @include('blog._form')
        {!! Form::close() !!}
    </div>
@stop
