<?php

use Illuminate\Database\Seeder;
use App\Blog;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call('BlogPostsSeeder');
    }
}

class BlogPostsSeeder extends Seeder
{
    public function run()
    {
        DB::table('blog_posts')->delete();

        Blog::create(array(
            'title' => 'Hello World!',
            'name' => 'user_1',
            'post' => 'My first post!',
            'created_at' => time(),
        ));

        Blog::create(array(
            'title' => 'New post',
            'name' => 'user_1',
            'post' => 'My second post!',
            'created_at' => time(),
        ));

        Blog::create(array(
            'title' => 'Another post',
            'name' => 'user_2',
            'post' => 'Here is another post',
            'created_at' => time(),
        ));
    }
}
