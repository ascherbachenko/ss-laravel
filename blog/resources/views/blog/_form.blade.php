<div class="form_group">
    {!! Form::label('name') !!}
    {!! Form::input('text', 'name', null, array('class' => 'form-control')) !!}
</div>

<div class="form_group">
    {!! Form::label('title') !!}
    {!! Form::input('text', 'title', null, array('class' => 'form-control')) !!}
</div>

<div class="form_group">
    {!! Form::label('post') !!}
    {!! Form::textArea('post', null, array('class' => 'form-control')) !!}
</div>

<div class="form_group">
    {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
</div>
