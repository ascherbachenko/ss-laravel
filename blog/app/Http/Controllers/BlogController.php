<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Blog;

class BlogController extends Controller
{
    public function index(Blog $blogModel)
    {
        $blog_posts = $blogModel->getAllPosts();

        return view('blog.index', array('blog_posts' => $blog_posts));
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Blog $blogModel, Request $request)
    {
        $post_data = $request->all();
        $post_data['created_at'] = $post_data['updated_at'] = time();
        $post_data['status'] = 1;
        unset($post_data['_token']);
        /* dd($post_data); */

        $blogModel->create($post_data);

        return redirect()->route('blog');
    }
}
