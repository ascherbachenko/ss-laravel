<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog_posts';
    protected $fillable = array(
        'name',
        'title',
        'status',
        'post',
        'created_at',
        'updated_at',
    );

    public function getAllPosts()
    {
        return self::all();
    }
}
