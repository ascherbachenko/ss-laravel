@extends('layouts.app')

@section('content')
<div class="container">
    {!! link_to_route('blog.create', 'Add') !!}

    @foreach ($blog_posts as $post)
        <article>
            <h2>{{ $post->title }}</h2>
            <p>{{ $post->post }}</p>
            <p>{{ $post->updated_at }}</p>
        </article>
    @endforeach
</div>
@stop
